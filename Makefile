install:
	go install ./...

test:
	go test -v ./...

fmt:
	gofmt -w .

build:
	mkdir -p build/linux_x64
	env GOOS=linux GOARCH=amd64 go build -o build/linux_x64/lsha ./...
	mkdir -p build/linux_arm
	env GOOS=linux GOARCH=arm go build -o build/linux_arm/lsha ./...
	mkdir -p build/win_x64
	env GOOS=windows GOARCH=amd64 go build -o build/win_x64/lsha.exe ./...

clean:
	rm -rf build

.PHONY: intall test fmt
