package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

func hashFile(paths_chan <-chan string, colors bool) {
	for path := range paths_chan {
		hash_sum := calcFileSHA256(path)

		var hash_str string
		if hash_sum != nil {
			hash_str = fmt.Sprintf("%64x", hash_sum)
		} else {
			hash_str = fmt.Sprintf("%64s", "err")
		}

		if colors {
			if hash_sum != nil {
				hash_str = formatColorStr(hash_str, ColorBlue)
			} else {
				hash_str = formatColorStr(hash_str, ColorRed)
			}
			path = formatColorStr(path, ColorGreen)
		}

		fmt.Printf("%s %s\n", hash_str, path)
	}
}

func scanDir(path string, paths_chan chan<- string, recursion_level uint32, recursion_limit uint32) {
	path_info, err := os.Lstat(path)
	checkErr(err)

	if !path_info.IsDir() && path_info.Mode().IsRegular() {
		paths_chan <- path
	} else {
		dir_files, err := ioutil.ReadDir(path)
		if err != nil {
			return
		}

		recursion_level++
		if recursion_level > recursion_limit {
			return
		}

		for _, file := range dir_files {
			file_full_path := filepath.Join(path, file.Name())
			scanDir(file_full_path, paths_chan, recursion_level, recursion_limit)
		}
	}
}
