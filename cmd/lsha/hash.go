package main

import (
	"bufio"
	"crypto/sha256"
	"io"
	"os"
)

func calcFileSHA256(path string) []byte {
	hash := sha256.New()

	file_handle, err := os.Open(path)
	if os.IsPermission(err) {
		return nil
	}
	checkErr(err)
	defer file_handle.Close()

	reader := bufio.NewReader(file_handle)
	_, err = io.Copy(hash, reader)
	if err != nil {
		return nil
	}

	return hash.Sum(nil)
}
