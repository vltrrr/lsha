package main

import (
	"fmt"
	"log"
)

// ____________________________________________________________________________
// error handling

func checkErr(err error) {
	if err != nil {
		fmt.Print("\n*** Unexpected error ***\n")
		log.Fatal(err)
	}
}

// ____________________________________________________________________________
// printout

const (
	ColorFmtStr = "\033[38;5;%dm%s\033[39;49m"
	ColorGreen  = 46
	ColorBlue   = 51
	ColorRed    = 197
)

func formatColorStr(msg string, color int) string {
	return fmt.Sprintf(ColorFmtStr, color, msg)
}
