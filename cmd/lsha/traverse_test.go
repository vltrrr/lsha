package main

import (
	"testing"
)

func TestHashFile(t *testing.T) {
	paths_chan := make(chan string)
	go hashFile(paths_chan, true)

	paths_chan <- "./testdata/sha256test.txt"
	close(paths_chan)
}
