package main

import (
	"fmt"
	"testing"
)

func TestCalcSHA256(t *testing.T) {
	test_sum := "3d9e250c3f5eea1a879dfe21d430f05081ea621f3b1a243e3ca256dfa654f793"
	hash_sum := calcFileSHA256("./testdata/sha256test.txt")
	if fmt.Sprintf("%x", hash_sum) != test_sum {
		t.Error("sha256 mismatch")
	} else {
		t.Log("shw256 ok")
	}
}
