package main

import (
	"flag"
	"fmt"
	"os"
	"sync"
)

// ____________________________________________________________________________
// global

const (
	VERSION       string = "0.3.2 2021_02_19"
	MAX_THREADS   int    = 64
	MAX_RECURSION uint32 = 4294967295 // max uint32 2**32 -1 :)
)

// ____________________________________________________________________________
// args

type lsha_args struct {
	root_path       string
	recursion_limit uint32
	threads         int
	colors          bool
	version         bool
}

func parseArgs() *lsha_args {
	levelsPtr := flag.Int("l", 1, "Directory levels to traverse")
	recursivePtr := flag.Bool("r", false, "Recursive mode")
	threadsPtr := flag.Int("t", 8, "Threads")
	versionPtr := flag.Bool("version", false, "Display version")

	flag.Parse()

	args := lsha_args{
		root_path:       ".",
		recursion_limit: uint32(*levelsPtr),
		threads:         *threadsPtr,
		colors:          false,
		version:         *versionPtr,
	}

	// check if stdout is a terminal
	if stdoutFileInfo, _ := os.Stdout.Stat(); (stdoutFileInfo.Mode() & os.ModeCharDevice) != 0 {
		args.colors = true
	}

	tail_args := flag.Args()
	if len(tail_args) > 0 {
		args.root_path = tail_args[0]
	}

	if *recursivePtr == true {
		args.recursion_limit = MAX_RECURSION // max uint32 2**32 -1 :)
	}

	if args.threads < 1 {
		args.threads = 1
	} else if args.threads > MAX_THREADS {
		args.threads = MAX_THREADS
	}

	return &args
}

// ____________________________________________________________________________
// main

func main() {
	args := parseArgs()

	if args.version {
		fmt.Printf("lsha %s\n", VERSION)
		return
	}

	paths_chan := make(chan string)
	go func() {
		scanDir(args.root_path, paths_chan, 0, args.recursion_limit)
		close(paths_chan)
	}()

	var wg sync.WaitGroup
	for i := 0; i < args.threads; i++ {
		wg.Add(1)
		go func() {
			hashFile(paths_chan, args.colors)
			wg.Done()
		}()
	}

	wg.Wait()
}
