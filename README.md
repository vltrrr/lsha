# lsha - parallel SHA256

![alt text](img/lsha_screenshot.png "lsha")

- **What?** **`lsha`** is **`ls` with sha256**. It lists a directory, calculating SHA256 hashes.
- **Parallel?** Hashes are calculated concurrently for several files using goroutines (by default 8 threads), speeding it up for at least SSD:s. Disable with `-t 1`
- **Why?** I was trying to learn Go, and I frequently check some downloads and other files with `sha256sum`. Go seems nice for CLI tools.


## Installation

You need go, probably anything higher than >1.14 works. Tested with `go1.16 linux/amd64`

```
go install gitlab.com/vltrrr/lsha@latest
```

## Usage

Just like `ls`

```
$ lsha
ee125ae604c8269390d112237c8ff715de0ed4e461a7b933c8f2e157596ac910  packer
3426f087403ce7d9bcf21d17c68c2df7e73c2ffc320c138e88a5768109be39a4  terraform
d5d964c848bfbd64c860b3e2f76b51abebc029974edf098121c54b37492f27f5  tflint
```

### Options

```
Usage of lsha:
  -l int
    	Directory levels to traverse (default 1)
  -r	Recursive mode
  -t int
    	Threads (default 8)
  -version
    	Display version

```

### Examples

```
# current dir
lsha

# list some other dir
lsha Downloads/

# recursively traverse all dirs
lsha -r

# recursively traverse 2 directory levels
lsha -l 2

```
